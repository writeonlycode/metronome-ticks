const Tone = require('tone/build/Tone.js');

class Countdown {
	constructor(initialMilliseconds, tickCallback , timeoutCallback) {
		this.initialMilliseconds = initialMilliseconds;
		this.remainingMilliseconds = initialMilliseconds;

		this.tickCallback = tickCallback;
		this.timeoutCallback = timeoutCallback;

		this.intervalId = null;
		this.startDate = null;
	}

	start() {
		this.startDate = Date.now();
		let thisCountdown = this;

		this.remainingMilliseconds = this.initialMilliseconds;

		this.intervalId = setInterval(() => {
			let elapsedMilliseconds = Date.now() - thisCountdown.startDate;
			this.remainingMilliseconds = this.initialMilliseconds - elapsedMilliseconds;

			if ( this.remainingMilliseconds > 0 ) {
				this.tickCallback();
			} else {
				this.remainingMilliseconds = 0;

				clearInterval(this.intervalId);
				this.intervalId = null;

				// create a synth and connect it to the main output (your speakers)
				const synth = new Tone.Synth().toDestination();
				const now = Tone.now();

				synth.triggerAttackRelease("C5", "8n", now);
				synth.triggerAttackRelease("C5", "8n", now + 0.25);
				synth.triggerAttackRelease("C5", "8n", now + 0.50);

				synth.triggerAttackRelease("C5", "8n", now + 2);
				synth.triggerAttackRelease("C5", "8n", now + 2.25);
				synth.triggerAttackRelease("C5", "8n", now + 2.50);

				synth.triggerAttackRelease("C5", "8n", now + 4);
				synth.triggerAttackRelease("C5", "8n", now + 4.25);
				synth.triggerAttackRelease("C5", "8n", now + 4.50);

				this.tickCallback();
				this.timeoutCallback();
			}
		}, 100);
	}

	stop() {
		clearInterval(this.intervalId);

		this.intervalId = null;
		this.startDate = null;

		this.remainingMilliseconds = this.initialMilliseconds;
		this.tickCallback();
	}

	increase() {
		if ( this.initialMilliseconds < (59 * 60000) && !this.intervalId ) {
			this.initialMilliseconds += 60000;
			this.remainingMilliseconds = this.initialMilliseconds;
			this.tickCallback();
		} else if ( this.initialMilliseconds < (59 * 60000) && this.intervalId ) {
			this.initialMilliseconds += 60000;
			this.remainingMilliseconds += 60000;
			this.tickCallback();
		}
	}

	decrease() {
		if ( this.initialMilliseconds > 60000 && !this.intervalId ) {
			this.initialMilliseconds -= 60000;
			this.remainingMilliseconds = this.initialMilliseconds;
			this.tickCallback();
		} else if ( this.initialMilliseconds > 60000 && this.intervalId ) {
			this.initialMilliseconds -= 60000;
			this.remainingMilliseconds -= 60000;
			this.tickCallback();
		}
	}

	toMinutesSeconds() {
		let secondsTotal = Math.floor(this.remainingMilliseconds / 1000);
		let minutes = Math.floor(secondsTotal / 60);
		let seconds = Math.floor(secondsTotal % 60);

		let formattedMinutes = ("0" + minutes).slice(-2);
		let formattedSeconds = ( "0" + seconds).slice(-2);

		return `${formattedMinutes}:${formattedSeconds}`;
	}
}

module.exports = Countdown;