Date.prototype.getCurrentWeekDay = function() {
	let days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
	let currentDay = this.getDay();

	return days[currentDay];
}

Date.prototype.getCurrentMonth = function() {
	let months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
	let currentMonth = this.getMonth();

	return months[currentMonth];
}

module.exports = Date;