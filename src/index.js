const Metronome = require('./metronome');
const Countdown = require('./countdown');
const Date = require('./date');

window.onload = function() {
	// Attach the event listeners
	document.getElementById("id-btn-clipboard").onclick = btnClipboarOnClick;
	
  document.getElementById("id-btn-minus").onclick = btnBpmMinusOnClick;
	document.getElementById("id-btn-plus").onclick = btnBpmPlusOnClick;
	document.getElementById("id-range").oninput = rangeBpmOnInput;
  
  document.getElementById("id-stress-checkbox").onchange = checkboxStressOnChange;
  document.getElementById("id-btn-minus-stress").onclick = btnBeatsPerBarMinusOnClick;
  document.getElementById("id-btn-plus-stress").onclick = btnBeatsPerBarPlusOnClick;

  document.getElementById("id-btn-minus-timer").onclick = btnTimerMinusOnClick;
	document.getElementById("id-btn-plus-timer").onclick = btnTimerPlusOnClick;

  document.getElementById("play-button").onclick = btnStartStopOnClick;

	// Update displays
	document.getElementById("id-bpm-display").value = metronome.bpm;
	document.getElementById("id-beats-per-bar").value = metronome.beatsPerBar;
	document.getElementById("id-timer-display").value = countdown.toMinutesSeconds();
}

// Create Instance of Metronome
const metronome = new Metronome(60, 4, true, beatCallback);

function beatCallback() {
	let dotsDiv = document.getElementById("id-stress-beats-dots");

	let clearDotHTML = `
		<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			<path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
		</svg>
		`.trim();

	let filledDotHTML = `
		<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
			<circle cx="8" cy="8" r="8"/>
		</svg>
		`.trim();

	if (metronome.currentBeat === 1 && metronome.stressFirstBeat) {
		if ( dotsDiv.children[metronome.beatsPerBar - 1] ) {
			dotsDiv.children[metronome.beatsPerBar - 1].outerHTML = clearDotHTML;
		}

		if ( dotsDiv.children[metronome.currentBeat - 1] ) {
			dotsDiv.children[metronome.currentBeat - 1].outerHTML = filledDotHTML;
		}
	} else {
		if ( dotsDiv.children[metronome.currentBeat - 2] ) {
			dotsDiv.children[metronome.currentBeat - 2].outerHTML = clearDotHTML;
		}

		if ( dotsDiv.children[metronome.currentBeat - 1] ) {
			dotsDiv.children[metronome.currentBeat - 1].outerHTML = filledDotHTML;
		}
	}
} 

console.log("Metronome loaded!");

// Copy to the clipboard
function btnClipboarOnClick() {
	let date = new Date();
	let clipboardContent = `${date.getCurrentWeekDay()}, ${date.getCurrentMonth()} ${date.getDate()}, ${date.getFullYear()}\n${metronome.bpm} bpm`;

	navigator.clipboard.writeText(clipboardContent).then(
		function() {
			displayAlert("Today's date and the current BPM have been successfully copied to the clipboard!");
		},
		function() { console.log("Ops... Something went wrong!"); }
	);
}

function displayAlert(string) {
	let alertHTML = `
	<div class="alert alert-success alert-dismissible fade show fixed-top" role="alert">
		${string}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	`;

	document.getElementById("id-metronome").insertAdjacentHTML("afterbegin", alertHTML);

	setTimeout(() => { $('.alert').alert('close'); }, 3000)
}

// BPMs
function btnBpmMinusOnClick() {
  metronome.decreaseBpm();
  document.getElementById("id-bpm-display").value = metronome.bpm; 
  document.getElementById("id-range").value = metronome.bpm;
}

function btnBpmPlusOnClick() {
  metronome.increaseBpm();
  document.getElementById("id-bpm-display").value = metronome.bpm; 
  document.getElementById("id-range").value = metronome.bpm;
}

function rangeBpmOnInput() {
	metronome.setBpm(this.value);
	document.getElementById("id-bpm-display").value = this.value; 
}

// Stress First Beat and Beats Per Bar
function checkboxStressOnChange() {
	metronome.stressFirstBeat = this.checked;
}

function btnBeatsPerBarMinusOnClick() {
	metronome.decreaseBeatsPerBar();
	setBeatsDots(metronome.beatsPerBar);
	document.getElementById("id-beats-per-bar").value = metronome.beatsPerBar;
}

function btnBeatsPerBarPlusOnClick() {
	metronome.increaseBeatsPerBar();
	setBeatsDots(metronome.beatsPerBar);
	document.getElementById("id-beats-per-bar").value = metronome.beatsPerBar;
}

function setBeatsDots(num) {
	let dotsDiv = document.getElementById("id-stress-beats-dots");

	// Remove all dots
	while ( dotsDiv.firstChild ) {
		dotsDiv.firstChild.remove();
	}

	// Create new dots
	while ( dotsDiv.childNodes.length < num ) {
		let dotHTML = `
			<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
				<path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
			</svg>
			`.trim()

		dotsDiv.insertAdjacentHTML("beforeend", dotHTML);
	}
}

// Start/Stop Button
function btnStartStopOnClick() {
	if ( !metronome.isRunning ) {
		metronome.start();
		countdown.start();
		this.innerHTML = "Stop";
	} else {
		metronome.stop();
		countdown.stop();

		setBeatsDots(metronome.beatsPerBar);
		this.innerHTML = "Start";
	}
}

// Countdown
const countdown = new Countdown(300000,
	function() {
		document.getElementById("id-timer-display").value = countdown.toMinutesSeconds();
	},
	function() {
		console.log("Ting ring ring ring!!!");
	});

function btnTimerMinusOnClick() {
	countdown.decrease();
}

function btnTimerPlusOnClick() {
	countdown.increase();
}