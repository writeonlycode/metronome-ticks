Metronome Ticks is a clean and simple metronome with a built-in countdown timer and the option to emphasize the first beat of a measure, as well as the functionality to automatically copy the date and the bpm to the clipboard for easy logging of your practice sessions.

It's written in HTML5, CSS3 and JavaScript. It uses Tone.js for the audio functionality and Bootstrap for the layout.

You can check it out here: https://writeonlycode.gitlab.io/metronome-ticks/